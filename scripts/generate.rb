require 'erb'
require 'yaml'
require 'fileutils'

TEMPLATE = ERB.new(File.read(File.expand_path('templates/test_case.md.erb', __dir__)), nil, '-')
PARTIALS_PATH = File.expand_path('partials', __dir__)

def generate(number, test_case)
  TEMPLATE.result_with_hash(
    number: number,
    title: test_case['title'],
    description: test_case['description'],
    video: test_case['video'],
    epics: test_case['epics'].to_a,
    issues: test_case['issues'].to_a,
    preparation: process_with_partials(test_case['preparation'].to_a),
    steps: process_with_partials(test_case['steps'].to_a).map { |step| [step['step'], step['expected_results'].to_a] },
    error_explanation: test_case['error'],
    result: test_case['error'].to_s.empty? ? '✅ success' : '❌ error'
  )
end

def process_with_partials(preparation_steps)
  preparation_steps.map do |step|
    next step if step['partial'].nil?

    replace_variables(
      read_yaml(File.expand_path("#{step['partial']}.yml", PARTIALS_PATH)),
      step['replace']
    )
  end.flatten
end

def replace_variables(object, variables)
  return object if variables.nil? || variables.empty?

  case object
  when String then replace_variables_for_string(object, variables)
  when Hash then object.transform_values { |value| replace_variables(value, variables) }
  when Array then object.map { |value| replace_variables(value, variables) }
  end
end

def replace_variables_for_string(string, variables)
  variables
    .reduce(string) { |memo, (name, value)| memo.gsub("$#{name}", value) }
end

def read_yaml(file_path)
  YAML.safe_load(File.read(file_path))
end

all_files = Dir['src/**/*.yml']
all_files.each.with_index do |file_path, index|
  puts "Processing #{file_path} (#{index + 1}/#{all_files.size})"
  destination_file_path = file_path.sub('src', 'dst').sub('.yml', '.md')
  FileUtils.mkdir_p(File.dirname(destination_file_path))
  file_content = File.read(file_path)
  File.open(destination_file_path, 'w+') { |f| f.write(generate(file_path[/\d{3}/], YAML.safe_load(file_content))) }
end

# Govern: Security Policies - Test Cases

This project contains manual test cases for features delivered by Govern: Security Policies team. Each test case has a list of steps, a video, and expected results after given actions. When a new feature is added from Govern: Security Policies team, we should have manual test coverage added to this project.

## How to add a new test case?

1. Fork or clone https://gitlab.com/gitlab-org/govern/security-policies/projects/test-cases/ project.
1. Open the project in [Gitpod](https://docs.gitlab.com/ee/integration/gitpod.html) or clone it to your local computer (make sure you have `ruby 2.7+` installed).
1. Create a new file in `src/scan-execution-policies`, `src/scan-result-policies` or `src/policies-list` with name format: `NNN-short-summary-of-test-case.yml`, where `NNN` is a following number in this list.
1. Copy `YAML` content from one of the other available policies, change `title`, `description`, `epics`, and `issues`.
1. Write preparation steps in `preparation` and test case steps in `steps` with expected results or reuse existing `partials`.
1. Using [kap](https://github.com/wulkano/Kap), record a video without audio where you record steps you performed in the given test case, change FPS to 10, nd lower the resolution to ~1000px to keep the video filesize small.
1. Add the recorded video to the appropriate directory in the `videos` directory.
1. Provide the path to the video in the `video` field in created `YAML` file.
1. Run the script to generate the `Markdown` file: `ruby scripts/generate.rb`.
1. Create a new commit and push your changes to create the merge request to this repository.
